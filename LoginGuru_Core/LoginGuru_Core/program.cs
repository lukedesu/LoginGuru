﻿// ci sono dei problemi con le date che si sovrappongono, considerando che sto usando il random per generare date non so come sistemare il tutto
using System;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace LoginGuru_Core
{
	class Program
	{
		private static IWebDriver Driver;
		static void Main(string[] args)
		{
			// WebDriver Setup
			Driver = new ChromeDriver(Variables.WebDriverPath);
			Driver.Manage().Window.Maximize();
			

			//Basic Login - Dashboard
			login();
			dashboard();
			part1();
			part2();
			part3();
			part4();
			part5();
			part6();
		}



		// Logs in the account
		static void login()
		{
			Driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");
			// Define elements in webpage
			var Username = Driver.FindElement(By.Name("txtUser"));
			var Password = Driver.FindElement(By.Name("txtPass"));
			var Btn = Driver.FindElement(By.Name("btnLogin"));
			// Input text, password and click login
			Username.SendKeys("g.suriani");
			Password.SendKeys("Smartpeg20");
			Btn.Click();
		}   // Fine Login
	
		static void dashboard()
		{
			clickOn(By.Id("ctl00_cphGuruMaster_272"));
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit"));
			Thread.Sleep(Variables.defaultTimeout);
		}	// Fine Dashboard

		static void part1()
		{
			
			ClearOn(By.Id("ctl00_cphGuruMaster_txtTelefono"));
            typeOn(By.Id("ctl00_cphGuruMaster_txtTelefono"), "123 456 7890");
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaeseRecapito"))).SelectByIndex(new Random().Next(1, 12));
            clickOn(By.Id("ctl00_cphGuruMaster_divButton"));
			Thread.Sleep(Variables.defaultTimeout);
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[2]/span/span/input"));
            Thread.Sleep(Variables.defaultTimeout);
		
			try{
				deleteElement1();
				newElement1();
				editElement1();
			} catch {
				newElement1();
				newElement1();
				editElement1();
				deleteElement1();
			}


			void newElement1()
        	{
            	int date = new Random().Next(1, 31);
            	int month = new Random().Next(6, 12);
            	int year = new Random().Next(2025, 2070);
            	Thread.Sleep(Variables.defaultTimeout);
           		try
            	{
                	clickOn(By.XPath("//table[@class='Tb']//input[@value='Nuovo']"));
            	} catch {
               		clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_elenco_gvRapporti_ctl01_btnNuovoEmpty"));
           		}

            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtMatricola"), RandomString(4));
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_ddlTipoRapporto"))).SelectByIndex(5);
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtDataInizio"), "27/06/2007");
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtDataFine"), date + "/" + month + "/" + year);
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnSalva"));
            	Thread.Sleep(Variables.defaultTimeout);

        	} // Fine New Element 1

			void editElement1()
        	{
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div/div/div[4]/table/tbody/tr[2]/td[1]/input[1]"));
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_txtAnnotazioni"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnSalva"));
        	}	// Fine Edit Element 1


			void deleteElement1()
        	{
				Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[1]/div/div/div/div[4]/table/tbody/tr[2]/td[1]/input[1]"));
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabRapportiLavoro_ucAnag_RapportiLavoro_ucAnag_RapportiLavoro_dettaglio_btnElimina"));
            	IAlert alert = Driver.SwitchTo().Alert();
				alert.Accept();
        	}	// Fine Delete Element 1

		} // Fine Parte 1

		static void part2(){
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            Thread.Sleep(Variables.defaultTimeout);
            clickOn(By.Id("__tab_ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale"));


			try{
				deleteElement2();
				newElement2();
				editElement2();
			} catch {
				newElement2();
				newElement2();
				editElement2();
				deleteElement2();
			}


			void newElement2(){
				string date = new Random().Next(1, 31) + "/" + new Random().Next(6, 12) + "/" + new Random().Next(2021, 2070);
            	int random = new Random().Next(1, 12);
            	var valore = new Random().Next(1, 100);
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("(//table[@class='Tb']//input[@value='Nuovo'][@type='button'])[2]"));
            	Thread.Sleep(Variables.defaultTimeout);
	
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_txtDataInizio"), "27/06/2007");
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_txtDataFine"), date);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_ddlCausale"))).SelectByIndex(random);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_ddlTipoValore"))).SelectByIndex(random);
            	Thread.Sleep(Variables.defaultTimeout);
            	new SelectElement(Driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[2]/div/div/div/div[3]/table/tbody/tr[5]/td[2]/select"))).SelectByIndex(1);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_txtPercentuale"), valore.ToString());
            	Console.WriteLine("Selected element " + random);
	
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_btnSalva"));
	
            	Thread.Sleep(Variables.defaultTimeout);

			} 	// Fine New Element 2

			void editElement2(){
				clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[2]/div/div/div/div[2]/div[2]/table/tbody/tr[2]/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_txtNote"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
           		clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_btnSalva"));
			}	// Fine Edit Element

			void deleteElement2(){
				Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[4]/div/div/div[2]/div[2]/div[2]/div/div/div/div[2]/div[2]/table/tbody/tr[2]/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabCurriculumAziendale_ucAnag_CurriculumAziendale_ucAnag_CurriculumAziendale_dettaglio_btnElimina"));
            	IAlert alert = Driver.SwitchTo().Alert();
				alert.Accept();
			}   // Fine Delete Element 2

		} 		// Fine Part 2

		static void part3(){
			Thread.Sleep(Variables.defaultTimeout);
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[3]/div[1]/div[3]/div/div/span[4]/span/span/input"));

			try{
				deleteElement3();
				newElement3();
				editElement3();
			} catch {
				newElement3();
				newElement3();
				editElement3();
				deleteElement3();
			}


			void newElement3(){
				string date = new Random().Next(1, 31) + "/" + new Random().Next(6, 12) + "/" + new Random().Next(2021, 2070);
				int causale = new Random().Next(1, 4);
            	var valore = new Random().Next(1, 100);
            	Thread.Sleep(Variables.defaultTimeout);        
                clickOn(By.XPath("(//table[@class='Tb']//input[@value='Nuovo'][@type='button'])[1]"));
            	Thread.Sleep(Variables.defaultTimeout);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoAltraEsperienza"))).SelectByIndex(causale);
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_txtDataInizioAltraEsperienza"), "27/06/2007");
            	typeOn(By.Id("ctl00_cphGuruMaster_txtDataFineAltraEsperienza"), date);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlLuogo"))).SelectByIndex(valore);
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
            	Thread.Sleep(Variables.defaultTimeout);
			}   // Fine NewElement 3

			void editElement3(){
				clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div[3]/div/fieldset/div[2]/div/div/table/tbody/tr[2]/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_txtNote"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            	clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
			} 	// Fine EditElement 3

			void deleteElement3(){
				Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div[3]/div/fieldset/div[2]/div/div/table/tbody/tr[2]/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_btnElimina"));
            	IAlert alert = Driver.SwitchTo().Alert();
            	alert.Accept();
			}	// Fine DeleteElement 3
		} 		// Fine Part 3


		static void part4(){
			Thread.Sleep(Variables.defaultTimeout);
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            eval.ExecuteScript("window.scroll(0,0)");
            clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[3]/div[3]/div/div/span[5]/span/span/input"));
		
			try{
				deleteElement4();
				newElement4();
				editElement4();
			 } catch {
				newElement4();
				editElement4();
				deleteElement4();
			}


			void newElement4(){
			string date = new Random().Next(1, 31) + "/" + new Random().Next(6, 12) + "/" + new Random().Next(2021, 2070);
			int causale = new Random().Next(1, 6);
            var valore = new Random().Next(1, 100);
            Thread.Sleep(Variables.defaultTimeout);
                        
            clickOn(By.XPath("//input[@value='Nuovo'][@type='button']"));
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_txtDataInizio"), "27/06/2007");
            typeOn(By.Id("ctl00_cphGuruMaster_txtDataFine"), date);     
            typeOn(By.Id("ctl00_cphGuruMaster_txtAzienda"), "test123"); 
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoSettoreAzienda"))).SelectByIndex(causale);   
            typeOn(By.Id("ctl00_cphGuruMaster_txtCitta"), "via dell'x");
            new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaese"))).SelectByIndex(valore);
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_txtTipoRapporto"), "-");
            typeOn(By.Id("ctl00_cphGuruMaster_txtMansioni"), "-");
            typeOn(By.Id("ctl00_cphGuruMaster_txtInquadramento"), "-");
            Thread.Sleep(Variables.defaultTimeout);
            typeOn(By.Id("ctl00_cphGuruMaster_txtRetribuzioneMensileNetta"), valore.ToString());
            typeOn(By.Id("ctl00_cphGuruMaster_txtRetribuzioneMensileLorda"), valore.ToString());
            clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
            clickOn(By.Id("ctl00_cphGuruMaster_btnIndietro"));
		
			}	// Fine New Element 4

			void editElement4(){
				clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/fieldset/div[2]/div/div[2]/div[1]/table/tbody/tr/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_txtAnnotazioni"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            	clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
            	clickOn(By.Id("ctl00_cphGuruMaster_btnIndietro"));
			}	// Fine Edit Element 4

			void deleteElement4(){
				Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/fieldset/div[2]/div/div[2]/div[1]/table/tbody/tr/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_btnElimina"));
            	IAlert alert = Driver.SwitchTo().Alert();
            	alert.Accept();
			}   // Fine Delete Elemnent 4
		} 		// Fine Part 4


		static void part5(){
			Driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[3]/div[3]/div/div/span[6]/span/span/input")).Click();
            Thread.Sleep(2000);

            try
             {
                //elimina
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_rptCurriculumEx_ctl01_btnModifica")).Click();
                Thread.Sleep(2000);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnElimina")).Click();
                Thread.Sleep(2000);
                IAlert alert = Driver.SwitchTo().Alert();
                alert.Accept();
                Thread.Sleep(2000);
                //nuovo
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnNuovo")).Click();
                Thread.Sleep(2000);
                //modifica
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataInizio")).SendKeys("10 / 09 / 2020");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataFine")).SendKeys("20/09/2020");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAzienda")).SendKeys("Azienda");
                new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoSettoreAzienda"))).SelectByIndex(2);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtCitta")).SendKeys("Perugia");
                new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaese"))).SelectByIndex(2);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtMansioni")).SendKeys("aaa");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAnnotazioni")).SendKeys("bbb");
                Thread.Sleep(2000);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnSalva")).Click();
                Thread.Sleep(2000);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnIndietro")).Click();
            }
            catch
            {             
				Console.WriteLine("oops! sembra che qualcosa sia andato storto!");
                //nuovo
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnNuovo")).Click();
                Thread.Sleep(2000);
                //modifica
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataInizio")).SendKeys("10 / 09 / 2020");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtDataFine")).SendKeys("20/09/2020");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAzienda")).SendKeys("Azienda");
                new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlTipoSettoreAzienda"))).SelectByIndex(2);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtCitta")).SendKeys("Perugia");
                new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaese"))).SelectByIndex(2);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtMansioni")).SendKeys("aaa");
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_txtAnnotazioni")).SendKeys("bbb");
                Thread.Sleep(2000);
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnSalva")).Click();
                //elimina
                Driver.FindElement(By.Id("ctl00_cphGuruMaster_btnElimina")).Click();
                Thread.Sleep(2000);
                IAlert alert = Driver.SwitchTo().Alert();
                alert.Accept();
                Thread.Sleep(2000);
            }   // Fine Try 5
			Console.WriteLine("Finished Part 5");
		} // Fine Parte 5


		static void part6(){

				IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
            	eval.ExecuteScript("window.scroll(0,0)");
            	clickOn(By.XPath("(//input[@type='submit'][@value='Retribuzione'])"));
				
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("(//span[@id='__tab_ctl00_cphGuruMaster_TabContainer_TabBenefit'])"));

				

				try{
					deleteElement6();
					newElement6();
					editElement6();
				} catch {
					newElement6();
					editElement6();
					deleteElement6();
				}


				
			void newElement6(){
				Console.WriteLine("creating...");
            	string date = new Random().Next(1, 31) + "/" + new Random().Next(6, 12) + "/" + new Random().Next(2021, 2070);;
            	int causale = new Random().Next(1, 2);
            	var valore = new Random().Next(1, 100);
            	Thread.Sleep(Variables.defaultTimeout);

	
            	clickOn(By.XPath("(//input[@value='Nuovo'][@type='button'])[1]"));
            	Thread.Sleep(Variables.defaultTimeout);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_ddlAmbitiBenefit"))).SelectByIndex(causale);  
            	Thread.Sleep(Variables.defaultTimeout);
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_ddlTipoStrumento"))).SelectByIndex(causale);  
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_txtDescrizione"), "placeholder");
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_txtIdentificativo"), "placeholder");
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_txtDataDal"), "27/06/2007");
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_txtDataAl"), date);     
            	new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_ddlModalitaAssegnazione"))).SelectByIndex(causale);
            	Console.WriteLine("Saving Started...");  
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_btnSalva"));
            	Console.WriteLine("Saving Started...");
			} // Fine NewElement 6
	
			void editElement6(){
				Console.WriteLine("editing...");
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[5]/div/div/div[1]/div[2]/table/tbody/tr[2]/td[1]/input"));
            	Thread.Sleep(Variables.defaultTimeout);
            	typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_txtNote"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_btnSalva"));
            	//clickOn(By.Id("ctl00_cphGuruMaster_btnIndietro"));
			}   // Fine Edit Element 6

			void deleteElement6(){
				Console.WriteLine("deleting...");
            	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[5]/div/div/div[1]/div[2]/table/tbody/tr[2]/td[1]/input"));
             	Thread.Sleep(Variables.defaultTimeout);
            	clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabBenefit_ucAnag_Benefit_ucAnag_Benefit_dettaglio_btnElimina"));
            	//IAlert alert = Driver.SwitchTo().Alert();
            	//alert.Accept();
           		Thread.Sleep(Variables.defaultTimeout);
			}	// Fine Delete Element 6
		} // Fine Part 6


		//--------------------------Utils-----------------------------------//
		
		
		// Random string
		private static Random random = new Random();
		public static string RandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string (Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
		}

		static void clickOn(By args)
		{
			Driver.FindElement(args).Click();
		}

		static void typeOn(By args, string typeElement)
		{
			Driver.FindElement(args).SendKeys(typeElement);
		}

		static void ClearOn(By args)
		{
			Driver.FindElement(args).Clear();
		}
	}
}