﻿// ci sono dei problemi con le date che si sovrappongono, considerando che sto usando il random per generare date non so come sistemare il tutto
using System;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace LoginGuru_pt18
{
	class Program
	{
		private static IWebDriver Driver;
		static void Main(string[] args)
		{
			Driver = new ChromeDriver(Variables.WebDriverPath);
			Driver.Manage().Window.Maximize();
			// Tasks
			login();
			Manager();
			

			Console.ReadLine();
		}

		static void login()
		{
			Driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");
			// Define elements in webpage
			var Username = Driver.FindElement(By.Name("txtUser"));
			var Password = Driver.FindElement(By.Name("txtPass"));
			var Btn = Driver.FindElement(By.Name("btnLogin"));
			// Input text, password and click login
			Username.SendKeys("g.suriani");
			Password.SendKeys("Smartpeg20");
			Btn.Click();
		}

		static void Manager()
		{
			//                                           Da rimuovere, Presente nel file originale                                                                      //
			clickOn(By.Id("ctl00_cphGuruMaster_272"));
			Thread.Sleep(Variables.defaultTimeout);
			clickOn(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit"));
			Thread.Sleep(Variables.defaultTimeout);                                                                     //
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
			eval.ExecuteScript("window.scroll(0,0)");
			clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[12]/span/span/input"));
            Thread.Sleep(Variables.defaultTimeout);
			Driver.FindElement(By.Id("ctl00_cphGuruMaster_ucDocumentiAllegati_AFUpload_ctl10")).SendKeys("C:\\Users\\gianluca.suriani\\source\\repos\\LoginGuru\\assets\\512.png");
			typeOn(By.Id("ctl00_cphGuruMaster_ucDocumentiAllegati_txtFUdescrizione"), "Lorem ipsum congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar");
			clickOn(By.Id("ctl00_cphGuruMaster_ucDocumentiAllegati_btnImporta"));
		}

		// Utils
		// Random string
		private static Random random = new Random();
		public static string RandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string (Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
		}

		static void clickOn(By args)
		{
			Driver.FindElement(args).Click();
		}

		static void typeOn(By args, string typeElement)
		{
			Driver.FindElement(args).SendKeys(typeElement);
		}

		static void ClearOn(By args)
		{
			Driver.FindElement(args).Clear();
		}
	}
}