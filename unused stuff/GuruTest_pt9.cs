﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace test9
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(@"C:\Users\Massi\Desktop\ccc");
            driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/");
            driver.Manage().Window.Maximize();
            //login
            IWebElement username = driver.FindElement(By.Id("txtUser"));
            username.SendKeys("f.chioccoloni");
            IWebElement pass = driver.FindElement(By.Id("txtPass"));
            pass.SendKeys("Smartpeg20");
            IWebElement login = driver.FindElement(By.Id("btnLogin"));
            login.Click();
            //anagrafica modifica volontariato apri
            driver.FindElement(By.Id("ctl00_cphGuruMaster_272")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[8]/span/span/input")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("__tab_ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari")).Click();
            Thread.Sleep(2000);

            try
            {
                //elimina
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_elenco_gwCondizioniParticolari_ctl02_btnModifica")).Click(); //apri
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_btnElimina")).Click();
                Thread.Sleep(2000);
                //nuovo
                driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[2]/div/div/div/div[1]/div/table/tbody/tr[3]/td[7]/input")).Click();
                Thread.Sleep(2000);
                //modifica
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtDataInizio")).SendKeys("10 / 09 / 2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtDataFine")).SendKeys("20/09/2020");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_ddlCondizioneParticolare"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtNote")).SendKeys("aaa");
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_btnSalva")).Click();
                Thread.Sleep(2000);

            }
            catch
            {
                //nuovo
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_elenco_gwCondizioniParticolari_ctl01_btnNuovoEmpty")).Click();
                Thread.Sleep(2000);
                //modifica
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtDataInizio")).SendKeys("10 / 09 / 2020");
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtDataFine")).SendKeys("20/09/2020");
                new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_ddlCondizioneParticolare"))).SelectByIndex(2);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_txtNote")).SendKeys("aaa");
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_btnSalva")).Click();
                Thread.Sleep(2000);
                //elimina
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_elenco_gwCondizioniParticolari_ctl02_btnModifica")).Click(); //apri
                Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabCondizioniParticolari_ucAnag_CondizioniParticolari_ucAnag_CondizioniParticolari_dettaglio_btnElimina")).Click();
                Thread.Sleep(2000);

            }


        }
    }

}