# LoginGuru

### Progresso
| N. Test | Funzione da testare                           | Modulo     | Stato          |Gestito da   |Creato il  |Causa Errore             |
| ------- | --------------------------------------------- | ---------- | -------------- |----------   |---------- |------------------------ |
| 1       | Rapporti di Lavoro->Rapporti di lavoro        | Anagrafica | ✅ Finito      |            |4/09/2020  |                         |
| 2       | Rapporti di Lavoro->Curriculum aziendale      | Anagrafica | ✅ Finito      |            |7/09/2020  |                         |
| 3       | Altre esperienze                              | Anagrafica | ✅ Finito      |            |7/09/2020  |                         |
| 4       | Altre esperienze professionali                | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 5       | Volontariato                                  | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 6       | Retribuzione->Benefit                         | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 7       | Retribuzione->Iscrizione sindacato            | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 8       | Altri dati->Categorie protette                | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 9       | Altri dati->Condizioni particolari            | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 10      | Salute e sicurezza->Dispositivi di protezione | Anagrafica | ✅ Finito      |            |8/09/2020  |                         |
| 11      | Salute e sicurezza->Visite mediche            | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |
| 12      | Salute e sicurezza->Vaccinazioni              | Anagrafica | ❌ Errore      |            |9/09/2020  | Malfunzione pagina web  |
| 13      | Salute e sicurezza->Infortuni                 | Anagrafica | ❌ Errore      |            |9/09/2020  | Malfunzione pagina web  |
| 14      | Salute e sicurezza->Malattie professionali    | Anagrafica | ❌ Errore      |            |9/09/2020  | Malfunzione pagina web  |
| 15      | Salute e sicurezza->Strumenti di lavoro       | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |
| 16      | Salute e sicurezza->Formazione obbligatoria   | Anagrafica | ❌ Errore      |            |9/09/2020  | Malfunzione pagina web  |
| 17      | Formazione->Formazione                        | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |
| 18      | Formazione->Istruzione (e lingue conosciute)  | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |
| 19      | Conoscenze e competenze->Conoscenze           | Anagrafica | ✅ Finito      |            |10/09/2020 |                         |
| 20      | Conoscenze e competenze->Competenze           | Anagrafica | ✅ Finito      |            |10/09/2020 |                         |
| 21      | Documenti allegati                            | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |
| 21      | Documenti allegati                            | Anagrafica | ✅ Finito      |            |9/09/2020  |                         |

| Nome    | Funzione da testare                           | Modulo     | Stato          |Gestito da   |Creato il  |Causa Errore             |
| ------- | --------------------------------------------- | ---------- | -------------- |----------   |---------- |------------------------ |
|Full     | -                                             | -          | ✅ Finito     |             |10/09/2020  |                         |

