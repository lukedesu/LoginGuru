﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace LoginGuru_ptTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(Variables.WebDriverPath);
            driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/");
            driver.Manage().Window.Maximize();
            //login
            IWebElement username = driver.FindElement(By.Id("txtUser"));
            username.SendKeys("f.chioccoloni");
            IWebElement pass = driver.FindElement(By.Id("txtPass"));
            pass.SendKeys("Smartpeg20");
            IWebElement login = driver.FindElement(By.Id("btnLogin"));
            login.Click();
            //anagrafica modifica apri
            driver.FindElement(By.Id("ctl00_cphGuruMaster_272")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[11]/span/span/input")).Click();
            Thread.Sleep(2000);

             try
             {

                //elimina
                int m = 2;
                int j = 1;

                while (j != 0)
                {
                    String ambitoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + m + "]/div[1]/table/tbody/tr/td[3]/span")).Text;
                    String tipoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + m + "]/div[1]/table/tbody/tr/td[4]/span")).Text;
                    IWebElement elimina = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + m + "]/div[1]/table/tbody/tr/td[2]/input"));

                    if (tipoSkill.Equals("Capacità relazionali") && ambitoSkill.Equals("Commerciale"))
                    {
                        elimina.Click();
                        IAlert alert = driver.SwitchTo().Alert();
                        alert.Accept();
                        j = 0;
                    }
                    else
                    {
                        m++;
                    }
                }
                var aggiungi = driver.FindElement(By.XPath("//div[@class = 'TabsControl_divTabHeader_TabBody']//input[@value = 'Aggiungi']"));
                aggiungi.Click();
                Thread.Sleep(2000);

                var commerciale = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_trvCompetenzen4"));
                commerciale.Click();
                Thread.Sleep(1000);
                var capacitaRelazionali = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_trvCompetenzen5CheckBox"));
                capacitaRelazionali.Click();
                Thread.Sleep(1000);
                var inserisci = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnInserisci"));
                inserisci.Click();
                Thread.Sleep(2000);

                int n = 2;
                int i = 1;

                while (i != 0)
                {
                    String ambitoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + n + "]/div[1]/table/tbody/tr/td[3]/span")).Text;
                    String tipoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + n + "]/div[1]/table/tbody/tr/td[4]/span")).Text;


                    if (tipoSkill.Equals("Capacità relazionali") && ambitoSkill.Equals("Commerciale"))
                    {
                        var livello = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + n + "]/div[1]/table/tbody/tr/td[5]/select"));
                        SelectElement selectLivello = new SelectElement(livello);
                        selectLivello.SelectByText("Buono");
                        Thread.Sleep(1000);
                        i = 0;
                    }
                    else
                    {
                        n++;
                    }
                }

                var salva = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnSalva"));
                salva.Click();
            }



            catch
            {
            //nuovo
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnAggiungi")).Click();
            Thread.Sleep(2000);
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_trvCompetenzen4")).Click();
               
                Thread.Sleep(2000);


                //driver.FindElement(By.XPath("(//div[@id='ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_panAlberoAlbero']//input[@type='checkbox'])[" + new Random().Next(1, 10) + "]")).Click();
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_trvCompetenzen5CheckBox")).Click(); 
                Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnInserisci")).Click();
            Thread.Sleep(2000);
                //modifica

                int h = 2;
                int k = 1;

                while (k != 0)
                {
                    String ambitoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + h + "]/div[1]/table/tbody/tr/td[3]/span")).Text;
                    String tipoSkill = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + h + "]/div[1]/table/tbody/tr/td[4]/span")).Text;


                    if (tipoSkill.Equals("Capacità relazionali") && ambitoSkill.Equals("Commerciale"))
                    {
                        var livello = driver.FindElement(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/div/div[2]/div[2]/div[1]/div/div/div/div[1]/div[2]/div[" + h + "]/div[1]/table/tbody/tr/td[5]/select"));
                        SelectElement selectLivello = new SelectElement(livello);
                        selectLivello.SelectByText("Buono");
                        Thread.Sleep(1000);
                        k = 0;
                    }
                    else
                    {
                        h++;
                    }
                }
                    Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnSalva")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnDettaglioHead")).Click();
            Thread.Sleep(2000);
            new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_ddlObsolescenza"))).SelectByIndex(2);
            Thread.Sleep(2000);
            new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_ddlPeriodoMese"))).SelectByIndex(9);
            Thread.Sleep(2000);
            new SelectElement(driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_ddlPeriodoAnno"))).SelectByIndex(1);
            Thread.Sleep(2000);
            IWebElement f = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_cboxOggi"));
            if (f.Selected == true)
            {
                driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_cboxOggi")).Click();
            }
            Thread.Sleep(5000);
            IWebElement a = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnFinoAlMeseUp"));
            a.Click();
            a.Click();
            Thread.Sleep(4000);
            IWebElement b = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnFinoAlAnnoDown"));
            b.Click();
            Thread.Sleep(2000);
            IWebElement c = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnEsercitataMesiUp"));
            c.Click();
            Thread.Sleep(2000);
            IWebElement d = driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnEsercitataAnniUp"));
            d.Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_txtAnnotazioniSkill")).SendKeys("aaa");
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_btnSalva")).Click();

            //elimina
            Thread.Sleep(2000);
            driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabSkill_ucAnag_SkillConoscenze_ucAnag_SkillConoscenze_elenco_rptSkill_ctl01_btnEliminaSkill")).Click();
            Thread.Sleep(2000);
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            Thread.Sleep(2000);




            }





        }
    }

}  