﻿// ci sono dei problemi con le date che si sovrappongono, considerando che sto usando il random per generare date non so come sistemare il tutto
using System;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace LoginGuru_pt10
{
	class Program
	{
		private static IWebDriver Driver;
		static void Main(string[] args)
		{
			Driver = new ChromeDriver(Variables.WebDriverPath);
			Driver.Manage().Window.Maximize();
			// Tasks
			login();
			tenthQuest();
			try
			{
				deleteElement();
				newElement();
				editElement();
			}
			catch
			{
				Console.WriteLine("Can't find any element!");
				newElement();
				newElement();
				editElement();
				deleteElement();
			}

			Console.ReadLine();
		}

		static void login()
		{
			Driver.Navigate().GoToUrl("https://gurudemo.azurewebsites.net/pages/login.aspx");
			// Define elements in webpage
			var Username = Driver.FindElement(By.Name("txtUser"));
			var Password = Driver.FindElement(By.Name("txtPass"));
			var Btn = Driver.FindElement(By.Name("btnLogin"));
			// Input text, password and click login
			Username.SendKeys("g.suriani");
			Password.SendKeys("Smartpeg20");
			Btn.Click();
		}

		static void tenthQuest()
		{
			//                                           Da rimuovere, Presente nel file originale                                                                      //
			clickOn(By.Id("ctl00_cphGuruMaster_272"));
			Thread.Sleep(Variables.defaultTimeout);
			clickOn(By.Id("ctl00_cphGuruMaster_gvResult_ctl02_lnkEdit"));
			Thread.Sleep(Variables.defaultTimeout);
			//ClearOn(By.Id("ctl00_cphGuruMaster_txtTelefono"));
			//typeOn(By.Id("ctl00_cphGuruMaster_txtTelefono"), "123 456 7890");
			//new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_ddlPaeseRecapito"))).SelectByIndex(new Random().Next(1, 12));
			//clickOn(By.Id("ctl00_cphGuruMaster_btnSalva"));
			//                                          Da rimuovere, Presente nel file originale                                                                       //
			IJavaScriptExecutor eval = (IJavaScriptExecutor)Driver;
			eval.ExecuteScript("window.scroll(0,0)");
			clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[1]/div[3]/div[1]/div[3]/div/div/span[9]/span/span/input"));
			Thread.Sleep(Variables.defaultTimeout);
		}

		static void newElement()
		{
			Console.WriteLine("creating...");
			int date = new Random().Next(1, 31);
			int month = new Random().Next(6, 12);
			int year = new Random().Next(2021, 2070);
			int causale = new Random().Next(1, 3);
			var valore = new Random().Next(1, 100);
			Thread.Sleep(Variables.defaultTimeout);
			clickOn(By.XPath("//input[@type='submit'][@title='inserisci un nuovo elemento']"));
			Thread.Sleep(Variables.defaultTimeout);
			new SelectElement(Driver.FindElement(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_ddlDispositivi"))).SelectByIndex(causale);
			typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_txtDataAssegnazione__txtData"), "20/05/2020");
			typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_txtDataScadenza__txtData"), date + "/" + month + "/" + year);
			Thread.Sleep(Variables.defaultTimeout);
			typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_txtIdentificativo"), RandomString(6));
			Console.WriteLine("Saving Started...");
			try
			{
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_ucDocumentiAllegati_panDetailHead"));
				Thread.Sleep(Variables.defaultTimeout);
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_btnSalva"));
				Console.WriteLine("Saving Started...");
			}
			catch
			{
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_ucDocumentiAllegati_panDetailHead"));
				Thread.Sleep(Variables.defaultTimeout);
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_btnSalva"));
				Console.WriteLine("Saving Started...");
			}
		}

		static void editElement()
		{
			Console.WriteLine("editing...");
			Thread.Sleep(Variables.defaultTimeout);
			clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/fieldset/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div/table/tbody/tr[2]/td[9]/input[1]"));
			Thread.Sleep(Variables.defaultTimeout);
			typeOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_txtAnnotazioni"), "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
			try
			{
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_btnSalva"));
			}
			catch
			{
				clickOn(By.Id("ctl00_cphGuruMaster_TabContainer_TabDispositivi_ucMS_DPIRisorsa_ucDocumentiAllegati_panDetailHead"));
			}

        }

        static void deleteElement()
			{
				Console.WriteLine("deleting...");
				Thread.Sleep(Variables.defaultTimeout);
				clickOn(By.XPath("/html/body/form/div[4]/section/section/div/div[5]/fieldset/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div/table/tbody/tr[2]/td[9]/input[2]"));
				IAlert alert = Driver.SwitchTo().Alert();
				alert.Accept();
				Thread.Sleep(Variables.defaultTimeout);
			}


		// Utils
		// Random string
		private static Random random = new Random();
		public static string RandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string (Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
		}

		static void clickOn(By args)
		{
			Driver.FindElement(args).Click();
		}

		static void typeOn(By args, string typeElement)
		{
			Driver.FindElement(args).SendKeys(typeElement);
		}

		static void ClearOn(By args)
		{
			Driver.FindElement(args).Clear();
		}
	}
}